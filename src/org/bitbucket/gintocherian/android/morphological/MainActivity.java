/*
 * Copyright (c) 2004-2014 Ginto Cherian
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This file handles the main activity and all the calls and notifications
 * from the algorithm methods. 
 */


package org.bitbucket.gintocherian.android.morphological;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import java.util.Observable;
import java.util.Observer;

import org.bitbucket.gintocherian.morphological.AlgorithmOutput;
import org.bitbucket.gintocherian.morphological.MatrixWrapper;
import org.bitbucket.gintocherian.morphological.Morphological;
import org.bitbucket.gintocherian.morphological.Morphological.OperationName;


public class MainActivity extends Activity implements Observer, OnItemSelectedListener {

	
	private Morphological morph;
	private static final String tag = "MainActivity";
	private Spinner spinner;
	
	private LinearLayout mFrame;
	private MorphView imageView;
	private MatrixWrapper<Bitmap> androidWrapper;
	private ProgressBar progressBar;
	private int progressCounter;
	private RadioGroup radioGroup;
	
	//Variables added to support case where
	//while thread is running the radiobutton
	//could be clicked
	private volatile boolean threadDisregard = false;
	private volatile boolean threadRunning = false;
	private volatile int selectedImage;

	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		spinner = (Spinner) findViewById(R.id.operations_spinner);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
		        R.array.operations_array, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);
		spinner.setOnItemSelectedListener(this);
		
		progressBar = (ProgressBar)findViewById(R.id.progress_bar);
		radioGroup = (RadioGroup)findViewById(R.id.imageGroup);
		
		mFrame = (LinearLayout) findViewById(R.id.imageFrame);
		// Changes the height and width to the specified *pixels*
		imageView = new MorphView( getApplicationContext(), mFrame.getX(), mFrame.getY());
		mFrame.addView(imageView);
	}

	 
	@Override
	protected void onResume() {
		super.onResume();
		setImage(R.drawable.button_1);
	}


	//Button code handlers
	public synchronized void onRadioButtonClicked(View view) {
	    // Is the button now checked?
	    boolean checked = ((RadioButton) view).isChecked();

	    if(threadRunning)
	    	threadDisregard = true;
	    
	    // Check which radio button was clicked
	    switch(view.getId()) {
	        case R.id.image_button_1:
	            if (checked)
	        		setImage(R.drawable.button_1);
	            break;
	        case R.id.image_button_2:
	            if (checked)
	        		setImage(R.drawable.button_2);
	            break;
	        case R.id.image_button_3:
	            if (checked)
	        		setImage(R.drawable.button_3);
	            break;
	        case R.id.image_button_4:
	            if (checked)
	        		setImage(R.drawable.button_4);
	            break;
	        case R.id.image_button_5:
	            if (checked)
	        		setImage(R.drawable.button_5);
	            break;
	        case R.id.image_button_6:
	            if (checked)
	        		setImage(R.drawable.button_6);
	            break;
	    }
	}
	
	private void setImage(int resID)
	{
		selectedImage = resID;
		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inScaled = false;		
		Bitmap input = BitmapFactory.decodeResource(getResources(), resID, opts);
		androidWrapper = new AndroidMatrixWrapper();
		androidWrapper.changeInput(input);
		AlgorithmOutput test = new AlgorithmOutput();
		test.input = androidWrapper.getInput();
	   	imageView.updateNow(test);
		
	}


	/*
	 * Call the operation to run
	 */
	@Override
	public synchronized void onItemSelected(AdapterView<?> parent, View v, int position,
			long rowId) {
		if(position > 0)
		{
			threadRunning = true;
			morph = new Morphological();
			morph.addObserver(this);
			
			switch(position){
            case 1:
            	morph.setParameters(androidWrapper, OperationName.EROSION, 200);
            	break;
            case 2:
            	morph.setParameters(androidWrapper, OperationName.DILATION, 200);
                break;
	        case 3:
            	morph.setParameters(androidWrapper, OperationName.OPENING, 200);
	            break;
	        case 4:
            	morph.setParameters(androidWrapper, OperationName.CLOSING, 200);
	            break;
	        case 5:
            	morph.setParameters(androidWrapper, OperationName.HITMISS, 200);
	            break;
	        case 6:
            	morph.setParameters(androidWrapper, OperationName.PRUNE, 200);
	            break;
	        case 7:
            	morph.setParameters(androidWrapper, OperationName.THIN, 200);
	            break;
	        case 8:
            	morph.setParameters(androidWrapper, OperationName.THICK, 200);
	            break;
	        case 9:
            	morph.setParameters(androidWrapper, OperationName.SKELETON, 200);
	            break;
	        case 10:
            	morph.setParameters(androidWrapper, OperationName.BOUNDARY, 200);
	            break;
	        case 11:
            	morph.setParameters(androidWrapper, OperationName.CONVEXHULL, 200);
                break;
			}
	        Thread thread = new Thread(morph);
	        thread.start();
	        
	        //Displaying the progressBar
	        progressCounter = 0;
	        progressBar.setIndeterminate(false);  
	        progressBar.setProgress(progressCounter);  
	        progressBar.setVisibility(View.VISIBLE);
	        spinner.setEnabled(false);
			
		}
		
	}


	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub
		
	}
	
	/*
	 * Method to receive notifications and display images
	 */
	@Override
	public synchronized void update(Observable arg0, Object arg1) {

		//thread needs to be stopped, but I will try the following
		if(threadDisregard)
		{
			morph.deleteObserver(this);
			androidWrapper.changeInput((AlgorithmOutput)arg1);
			runOnUiThread(new Runnable() {  
                @Override
                public void run() {
        			progressBar.setVisibility(View.INVISIBLE);
        			spinner.setEnabled(true);
        	        spinner.setSelection(0);
        	        threadRunning = false;
        	        setImage(selectedImage);
                }
            });
			threadDisregard = false;
		}
		
		
		imageView.updateNow((AlgorithmOutput)arg1);
		progressBar.setProgress(++progressCounter);  
		
		//Algorithm has finished running
		if(((AlgorithmOutput)arg1).complete)
		{
			morph.deleteObserver(this);
			androidWrapper.changeInput((AlgorithmOutput)arg1);
			runOnUiThread(new Runnable() {  
                @Override
                public void run() {
        			progressBar.setVisibility(View.INVISIBLE);
        			spinner.setEnabled(true);
        	        spinner.setSelection(0);
        	        threadRunning = false;
                }
            });
		}
		//Log.i(tag, "Update recieved");
		
	}

	/*
	 * Class to draw the images
	 */
	private class MorphView extends View{

		private final Paint mPainter = new Paint();
		private float mXPos, mYPos;
		private Bitmap image;
		private static final String tag = "MorphView";
		
		public MorphView(Context context, float x, float y) {
			super(context);

			mXPos = x;
			mYPos = y;
			mPainter.setAntiAlias(true);
			
			this.invalidate();
		}
		public synchronized void updateNow(final AlgorithmOutput output) {			
			this.post(new Runnable() {
		        public void run() {
					AndroidMatrixWrapper wrapper = new AndroidMatrixWrapper();
					wrapper.changeInput((AlgorithmOutput)output);
					image = wrapper.getOutput();
					MorphView.this.invalidate();
					//Log.i(tag, "updateNow called ");
		        } 
		    });			
		}

		@Override
		protected synchronized void onDraw(Canvas canvas) {

			canvas.save(Canvas.MATRIX_SAVE_FLAG);
			if(image != null)
			{
				canvas.drawBitmap(image, mXPos, mYPos, mPainter);
			}
			canvas.restore();
			//Log.i(tag, "onDraw called");			
		}
	}
}
