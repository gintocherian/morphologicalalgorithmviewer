/*
 * Copyright (c) 2004-2014 Ginto Cherian
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Implementation of Matrix Wrapper for Android
 */

package org.bitbucket.gintocherian.android.morphological;

import org.bitbucket.gintocherian.morphological.AlgorithmOutput;
import org.bitbucket.gintocherian.morphological.MatrixHelper;
import org.bitbucket.gintocherian.morphological.MatrixWrapper;

import android.graphics.Bitmap;
import android.graphics.Color;

public class AndroidMatrixWrapper implements MatrixWrapper<Bitmap>{

	//50 X 50 array
	private int[][] inputMatrix;
	
	//400 X 400 image
	private Bitmap displayImage;
	
	private static final int inputSize = 50;
	private static final int outputSize = 400;
	
	public AndroidMatrixWrapper()
	{
	}
	
	public int[][] getInput() {
		return inputMatrix;
	}

	public Bitmap getOutput() {
		return displayImage;
	}
	
	//Constructor from interface side
	public void changeInput(Bitmap input)
	{
		setInput(input);
		setDisplayMatrix(inputMatrix);
		
	}

	//Constructor from algorithm side
	public void changeInput(AlgorithmOutput output)
	{
		setDisplayMatrix(output.input);
		
	}
	//Method to convert pixels to array
	private void setInput(Bitmap input) {
		inputMatrix = new int[inputSize][inputSize];
		for (int x = 0; x < inputSize; x++) {
			for (int y = 0; y < inputSize; y++) {
				if(input.getPixel(x, y) == Color.BLACK)
					inputMatrix[x][y] = 0;
				else
					inputMatrix[x][y] = 1;
			}
		}
	}


	//Convert array to pixels
	private void setDisplayMatrix(int[][] displayMatrix) {
		//enlarge matrix
		int[][] enlarged = MatrixHelper.enlargeMatrixEightTimes(displayMatrix, inputSize);
		//convert to argb values
		displayImage = Bitmap.createBitmap(outputSize, outputSize, Bitmap.Config.ARGB_8888);
		for (int x = 0; x < outputSize; x++) {
			for (int y = 0; y < outputSize; y++) {
				if(enlarged[x][y] == 1)
					displayImage.setPixel(x, y, Color.WHITE);
				else
					displayImage.setPixel(x, y, Color.BLACK);
			}
		}

	}


}
