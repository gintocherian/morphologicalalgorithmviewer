/*
 * Copyright (c) 2004-2014 Ginto Cherian
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This file contains algorithms developed based on descriptions in,
 * Digital Image Processing (2nd Edition) 
 *		Rafael C. Gonzalez (Author), Richard E. Woods (Author)
 *
 * This code has not been tested for arbitarily sized inputs and kernels
 * though the intention has been there. It has been tested for cases  
 * where,
 * 	Input = 50 X 50 matrix
 *  Display Output = 400 X 400 matrix or pixels
 *  Kernel = 3 X 3 matrix
 *  
 *  Currently modified to pure class implementing algorithms to avoid
 *  android compatibility issues
 */

package org.bitbucket.gintocherian.morphological;

import java.util.Observable;


public class Morphological  extends Observable implements Runnable{
	
	private static final int inputSize = 50;
	private int[][] inputArray;
	private static final int kernelSize = 3;
	
	private static final int[][] defaultKernel = {  
		{ 0,  1,  0 }, 
		{ 1,  1,  1 }, 
		{ 0,  1,  0 } 
	 }; 

	private static final int[][] skeletonKernel = {  
		{ 1,  1,  1 }, 
		{ 1,  1,  1 }, 
		{ 1,  1,  1 } 
	 }; //Skeleton

	private static final int[][] hitMissKernel = {  
		{ 0, -1, -1 }, 
		{ 1,  1, -1 }, 
		{ 0,  1,  0 } 
	 }; // hit-miss

	private static final int[][] thickeningKernel = { 
		{ 0, -1, -1 }, 
		{ 1,  0, -1 },
		{ 1,  1,  0 } 
	};// thick

	private static final int[][] convexHullKernel = { 
		{ 1,  0, 0 }, 
		{ 1,  0, 0 }, 
		{ 1,  0, 0 } 
	};// convexHull

	private static final int[][] pruneKernel1 = {   
		{ 0, -1, -1 }, 
		{ 1,  1, -1 }, 
		{ 0, -1, -1 } 
	};// prune
	private static final int[][] pruneKernel2 = {   
		{  1, -1, -1 }, 
		{ -1,  1, -1 },
		{ -1, -1, -1 } 
	};// prune
	
	
	//Variable required for thread
	public enum OperationName	{	DILATION, EROSION, HITMISS, CLOSING,
									OPENING, PRUNE,	BOUNDARY, THIN,
									THICK, CONVEXHULL, SKELETON};	
	private OperationName currentOperation;
	private int currentDelay;
								
	/*
	 * Empty Constructor
	 */
	public Morphological()
	{
	}
	
	/*
	 * Resets the object for next run
	 * Assuming only one thread operation at a time
	 */
	public void setParameters(MatrixWrapper<?> input, OperationName operation, int delay)
	{
		inputArray = input.getInput();
		currentOperation = operation;
		currentDelay = delay;	
	}

	/*
	 * Sends notification to display image to observer
	 * 
	 * Thread delay is put here, 
	 * hopefully no interruption
	 */
	private void sendNotification(int[][] input, boolean show, boolean complete)
	{
		if(show) {
			try {
				Thread.sleep(currentDelay);
			} catch (InterruptedException e) {}

			AlgorithmOutput currentOutput = new AlgorithmOutput();
			currentOutput.input = input;
			currentOutput.complete = complete;
			setChanged();
			notifyObservers(currentOutput);
		}
	}

	/*
	 * Run for a single operation and then end the thread
	 * 
	 */
	@Override
	public void run() {
		//Run a single operation and then end
		switch (currentOperation) {
			case DILATION:
	            dilation();
	            break;
			case EROSION:
	            erosion();
	            break;
			case HITMISS:
	            hitMiss();
	            break;
			case CLOSING:
	            closing();
	            break;
			case OPENING:
	            opening();
	            break;
			case PRUNE:
	            prune();
	            break;
			case BOUNDARY:
	            boundary();
	            break;
			case THIN:
	            thin();
	            break;
			case THICK:
	            thick();
	            break;
			case CONVEXHULL:
	            convexHull();
	            break;
			case SKELETON:
	            skeleton();
	            break;
		}
	
	}

	/*
	 * Finds the number of times an image has to be eroded before it
	 * becomes empty for skeleton operation
	 */
	private int getErosionCount(int[][] input, int[][] kernel) {
		int[][] inputCopy = new int[inputSize][inputSize];
		int n = 0;

		for (int i = 0; i < inputSize; i++)
			System.arraycopy(input[i], 0, inputCopy[i], 0, inputSize);

		while (MatrixHelper.checkNonZeroPresent(inputCopy, inputSize)) {
			n = n + 1;
			inputCopy = erosion(inputCopy, false, kernel);
		}
		return n;

	}


	/*
	 * Implements the thining operation 1-iteration
	 */
	private int[][] thin(int[][] input, int[][] kernel, int inputSize) {
		int[][] hitMissOutput = hitMiss(input, kernel, inputSize, kernelSize);
		int[][] output = new int[inputSize][inputSize];
		for (int i = 0; i < inputSize; i++)
			for (int j = 0; j < inputSize; j++) {
				if (input[i][j] != hitMissOutput[i][j])
					output[i][j] = 0;
				else
					output[i][j] = 1;
			}

		return output;
	}


	/*
	 * Implements a single iteration of the thickening operation
	 */
	private int[][] thick(int[][] input, int[][] kernel, int size) {
		int[][] clearedHitMiss = MatrixHelper.clearBorder(hitMiss(input, kernel, inputSize, kernelSize), size);
		int[][] output = new int[size][size];
		for (int i = 0; i < size; i++)
			for (int j = 0; j < size; j++) {
				output[i][j] = input[i][j] & clearedHitMiss[i][j];
			}
		return output;
	}

	/*
	 * Implements the Hit-Miss operation which is used for shape detection
	 */
	private int[][] hitMiss(int[][] input, int[][] kernel, int inputSize, int kernelSize) {
		int[][] positiveKernel = MatrixHelper.extractPositiveKernel(kernel, kernelSize);
		int[][] negativeKernel = MatrixHelper.extractNegativeKernel(kernel, kernelSize);
		int[][] inputInverted = MatrixHelper.complement(input, inputSize);

		int[][] erodedCopy = erosion(input, false, positiveKernel);
		int[][] erodedInverted = erosion(inputInverted, false, negativeKernel);
		int[][] output = new int[inputSize][inputSize];
		for (int i = 0; i < inputSize; i++) {
			for (int j = 0; j < inputSize; j++) {
				if (erodedCopy[i][j] == 0 && erodedInverted[i][j] == 0)
					output[i][j] = 0;
				else
					output[i][j] = 1;
			}
		}
		return output;
	}

	/*
	 * Implements the dilation operation, Square mask
	 * The output is fully initialized as the input to
	 * allow the image to be displayed properly
	 */
	private int[][] dilation(int[][] input, boolean show, int[][] kernel, int size) {
		int[][] output = new int[size][size];
		int deviation = kernelSize / 2, dilatedValue;
		for (int i = 0; i < size; i++)
			System.arraycopy(input[i], 0, output[i], 0, size);

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				//Run the kernel over each pixel
				dilatedValue = 1;
				for (int m = i - deviation, k = 2; m <= i + deviation; m++, k--) {
					for (int n = j - deviation, l = 2; n <= j + deviation; n++, l--) {
						if (m >= 0 && n >= 0 && m < inputSize && n < inputSize) {
							if (kernel[k][l] == 1) {
								dilatedValue = dilatedValue & input[m][n];
							}
						}
					}
				}
				output[i][j] = dilatedValue;
			}
			if (show)
				sendNotification(output, true, false);
		}
		return output;
	}

	/*
	 * Implements the erode operation using Square mask
	 * The output is fully initialized as the input to
	 * allow the image to be displayed properly
	 */
	private int[][] erosion(int[][] input, boolean show, int[][] kernel) {
		int[][] output = new int[inputSize][inputSize];
		
		int deviation = kernelSize / 2, erodedValue;
		for (int i = 0; i < inputSize; i++)
			System.arraycopy(input[i], 0, output[i], 0, inputSize);

		for (int i = 0; i < inputSize; i++) {
			for (int j = 0; j < inputSize; j++) {
				//Run the kernel over each pixel
				erodedValue = 0;
				for (int m = i - deviation, k = 2; m <= i + deviation; m++, k--) {
					for (int n = j - deviation, l = 2; n <= j + deviation; n++, l--) {
						if (m >= 0 && n >= 0 && m < inputSize && n < inputSize) {
							if (kernel[k][l] == 1) {
								erodedValue = erodedValue | input[m][n];
							}
						}
					}
				}
				output[i][j] = erodedValue;

			}
			if (show)
				sendNotification(output, true, false);
		}
		return output;
	}
	
	/*
	 * Generate the convex Hull
	 * This is a long running operation hence shorter delay
	 */
	private int[][] convexHull(int[][] input, int size, int[][] kernel, int kernelSize, boolean show) {
			
		int[][] convexHull = new int[size][size];
		int[][] oldConvexHull = new int[size][size];
		int[][] erodedMatrix = new int[size][size];
		int[][] rotationMatrix = new int[kernelSize][kernelSize];
		boolean matching = true;
		for (int i = 0; i < size; i++) {
			System.arraycopy(input[i], 0, convexHull[i], 0, size);
			System.arraycopy(input[i], 0, oldConvexHull[i], 0, size);
		}
		
		while(matching)
		{
			//Do operations for all 4 rotations of kernel
			for (int i = 0; i < 4; i++) {
				matching = true;
				rotationMatrix = MatrixHelper.rotateMatrix(kernel, kernelSize, i);
				erodedMatrix = erosion(convexHull, false, rotationMatrix);
				convexHull = MatrixHelper.union(erodedMatrix, convexHull, size);
				convexHull = MatrixHelper.clearBorder(convexHull, size);
				sendNotification(convexHull, show, false);
			}
			//Compare current & old
			matching = !MatrixHelper.compareMatrices(convexHull, oldConvexHull, size);
			//Update old
			for (int i = 0; i < size; i++){
				System.arraycopy(convexHull[i], 0, oldConvexHull[i], 0, size);
			}
				
		}

		return convexHull;

	}

	/*
	 * Does the boundary operation which is the 
	 * Boundary = difference of source image and eroded image
	 */
	private int[][] boundary(int[][] input, int size, int[][] kernel, int kernelSize, boolean show) {
		int[][] erodedImage = erosion(input, false, kernel);
		sendNotification(erodedImage, show, false);
		return MatrixHelper.difference(input, erodedImage, size);
	}

	/*
	 * Does the thinning operation 
	 * Rotates the matrix  and does the thinning 8 times
	 * The output is displayed on each thinning of image
	 */
	private int[][] thin(int[][] input, int size, int[][] kernel, int kernelSize, boolean show) {
		int[][] thinned = new int[size][size];
		int[][] rotatedKernel = new int[kernelSize][kernelSize];
		for (int i = 0; i < size; i++) {
			System.arraycopy(input[i], 0, thinned[i], 0, size);
		}
		for (int i = 1; i <= 8; i++) {
			rotatedKernel = rotateMatrix(kernel, i);
			thinned = thin(thinned, rotatedKernel, size);
			sendNotification(thinned, show, false);
		}
		
		return thinned;
	}

	/*
	 * Does the thickning operation
	 * Rotates the matrix  and does the thickning 8 times
	 * The output is displayed on each thickning of image
	 */
	private int[][] thick(int[][] input, int size, int[][] kernel, int kernelSize, boolean show) {
		int[][] thickened = new int[size][size];
		int[][] rotatedKernel = new int[kernelSize][kernelSize];
		for (int i = 0; i < size; i++) {
			System.arraycopy(input[i], 0, thickened[i], 0, size);
		}
		for (int i = 1; i <= 8; i++) {
			rotatedKernel = rotateMatrix(kernel, i);
			thickened = thick(thickened, rotatedKernel, size);
			sendNotification(thickened, show, false);
		}

		return thickened;
	}
	
	/*
	 * Makes the skeleton of an Image
	 * Skeleton = Skeleton + (erosion(i) - open(erosion(i))
	 */
	private int[][] skeleton(int[][] input, int size, int[][] kernel, int kernelSize, boolean show) {
		int[][] sumSkeleton = new int[size][size];
		int[][] currentSkeleton = new int[size][size];
		int[][] eroded = new int[size][size];
		int[][] opened = new int[size][size];
		for (int i = 0; i < size; i++)
			for (int j = 0; j < size; j++) {
				sumSkeleton[i][j] = 1;
				eroded[i][j] = input[i][j];
			}
		
		//Finding the maximum erosion before image disappears
		int erosionCount = getErosionCount(input, kernel);

		for (int i = 0; i < erosionCount; i++) {
			//Doing erosion
			if(i > 0)eroded = erosion(eroded, false, kernel);
			
			//Doing opening of erosion
			opened = dilation(erosion(eroded, false, kernel), false, kernel, size);

			currentSkeleton = MatrixHelper.difference(eroded, opened, size);
			sumSkeleton = MatrixHelper.union(sumSkeleton, currentSkeleton, size);
			sendNotification(sumSkeleton, show, false);
		}

		return sumSkeleton;

	}

	/*
	 * Implements the pruning operation
	 * This method needs more testing
	 */
	private int[][] prune(int[][] input, int size) {
		int[][] X1 = new int[inputSize][inputSize];
		int[][] X2 = new int[inputSize][inputSize];
		int[][] X3 = new int[inputSize][inputSize];
		int[][] pruned = new int[inputSize][inputSize];
		int[][] pruneKernel = new int[kernelSize][kernelSize];
		for (int i = 0; i < size; i++)
			for (int j = 0; j < size; j++) {
				X1[i][j] = input[i][j];
				X2[i][j] = 1;
			}
		
		//Preparing X1 = XO{B} i.e. thinnning
		for (int i = 1; i <= 8; i++) {
			pruneKernel = getPruneKernel(i);
			X1 = thin(X1, pruneKernel, inputSize);
		}

		//preparing X2
		for (int i = 1; i <= 8; i++) {
			pruneKernel = getPruneKernel(i);
			X2 = MatrixHelper.union(X2, hitMiss(X1, pruneKernel, size, kernelSize), size);
		}

		//preparing X3, only doing 1 dilation
		X3 = MatrixHelper.intersection(dilation(X2, false, skeletonKernel, size), 
				inputArray, size);

		pruned = MatrixHelper.union(X1,X3, size);

		return pruned;
	}

	/*
	 * Does the opening operation which is
	 * erosion followed by dilation
	 */
	private int[][] opening(int[][] input, int size, int[][] kernel, int kernelSize, boolean show) {
		int[][] erodedImage = erosion(input, false, kernel);		
		sendNotification(erodedImage, show, false);
		return dilation(erodedImage, false, kernel, size);
	}

	/*
	 * Does the closing operation which is dilation 
	 * followed by erosion
	 */
	private int[][] closing(int[][] input, int size, int[][] kernel, int kernelSize, boolean show) {
		int[][] dilatedImage = dilation(input, false, kernel, size);		
		sendNotification(dilatedImage, show, false);
		return erosion(dilatedImage, false, kernel);
	}

	/*
	 * Rotates a 3X3 matrix by using the center as a pivot in counter clockwise
	 * and moving the elements one by one like a circular matrix 
	 * n = 0 or less than 8
	 *
	 */
	private int[][] rotateMatrix(int[][] input, int n) {
		
		//Return input when n out of bounds or 0 
		if(n < 0 || n > 8 || n == 0 ) return input;
		
		int[][] rotatedMatrix = new int[kernelSize][kernelSize];
		int[][] rotatedMatrixCopy = new int[kernelSize][kernelSize];

		for (int i = 0; i < kernelSize; i++){
			System.arraycopy(input[i], 0, rotatedMatrixCopy[i], 0, kernelSize);
		}
		
		for(int i = 0; i < n; i++){
			rotatedMatrix[0][0] = rotatedMatrixCopy[0][1];
			rotatedMatrix[0][1] = rotatedMatrixCopy[0][2];
			rotatedMatrix[0][2] = rotatedMatrixCopy[1][2];
			rotatedMatrix[1][2] = rotatedMatrixCopy[2][2];
			rotatedMatrix[2][2] = rotatedMatrixCopy[2][1];
			rotatedMatrix[2][1] = rotatedMatrixCopy[2][0];
			rotatedMatrix[2][0] = rotatedMatrixCopy[1][0];
			rotatedMatrix[1][0] = rotatedMatrixCopy[0][0];
			rotatedMatrix[1][1] = rotatedMatrixCopy[1][1];

			//Saving previous
			for (int j = 0; j < kernelSize; j++){
				System.arraycopy(rotatedMatrix[j], 0, rotatedMatrixCopy[j], 0, kernelSize);
			}
			
		}
		return rotatedMatrix;
		
	}

	/*
	 * Rotates pruneMatrix1 and pruneMatrix2 in 90 degree increments 
	 */	
	private int[][] getPruneKernel(int n) {
		int[][] output = new int[kernelSize][kernelSize];
		switch (n) {
		case 1:
			output = rotateMatrix(pruneKernel1, 1);
			break;
		case 2:
			output = rotateMatrix(pruneKernel1, 3);
			break;
		case 3:
			output = rotateMatrix(pruneKernel1, 5);
			break;
		case 4:
			output = rotateMatrix(pruneKernel1, 7);
			break;
		case 5:
			output = rotateMatrix(pruneKernel2, 1);
			break;
		case 6:
			output = rotateMatrix(pruneKernel2, 3);
			break;
		case 7:
			output = rotateMatrix(pruneKernel2, 5);
			break;
		case 8:
			output = rotateMatrix(pruneKernel2, 7);
			break;
		default: // nothing
			break;
		}

		return output;
	}	
	

	/*
	 * Copy an array back to the input Array - in case of non-thread calls
	 * Send notification to observer 
	 */	
	private void finishOperation(int[][] processOutput)
	{
		for (int i = 0; i < inputSize; i++)
			System.arraycopy(processOutput[i], 0, inputArray[i], 0, inputSize);

		sendNotification(inputArray, true, true);
		
	}
		
	/*
	 * Call for dilation
	 * Change to protected to use thread setRuntime
	 */
	protected void dilation() {
		finishOperation(dilation(inputArray, true, defaultKernel, inputSize));
	}
	
	/*
	 * Call for erosion
	 * Change to protected to use thread setRuntime
	 */
	protected void erosion() {
		finishOperation(erosion(inputArray, true, defaultKernel));
	}

	/*
	 * Call for hitMiss
	 * Change to protected to use thread setRuntime
	 */
	protected void hitMiss() {
		finishOperation(hitMiss(inputArray, hitMissKernel, inputSize, kernelSize));
	}
	
	/*
	 * Call for closing
	 * Change to protected to use thread setRuntime
	 */
	protected void closing() {		
		finishOperation(closing(inputArray, inputSize, defaultKernel, kernelSize, true));
	}

	/*
	 * Call for opening
	 * Change to protected to use thread setRuntime
	 */
	protected void opening() {		
		finishOperation(opening(inputArray, inputSize, defaultKernel, kernelSize, true));
	}

	/*
	 * Call for prune
	 * Change to protected to use thread setRuntime
	 */
	protected void prune() {
		finishOperation(prune(inputArray, inputSize));
	}	

	/*
	 * Call for boundary
	 * Change to protected to use thread setRuntime
	 */
	protected void boundary() {
		finishOperation(boundary(inputArray, inputSize, defaultKernel, kernelSize, true));
	}
	
	/*
	 * Call for thinOperation
	 * Change to protected to use thread setRuntime
	 */
	protected void thin() {
		finishOperation(thin(inputArray, inputSize, hitMissKernel, kernelSize, true));
	}

	/*
	 * Call for thickOperation
	 * Change to protected to use thread setRuntime
	 */
	protected void thick() {
		finishOperation(thick(inputArray, inputSize, thickeningKernel, kernelSize, true));
	}

	/*
	 * Call for convexHull
	 * Change to protected to use thread setRuntime
	 */
	protected void convexHull() {
		finishOperation(convexHull(inputArray, inputSize, convexHullKernel, kernelSize, true));
	}
	
	/*
	 * Call for skeleton
	 * Change to protected to use thread setRuntime
	 */
	protected void skeleton() {
		finishOperation(skeleton(inputArray, inputSize, skeletonKernel, kernelSize, true));
	}
	
	/*
	 * Override generic implementation
	 */
	public String toString()
	{
		return MatrixHelper.printMatrix(inputArray, inputSize, "Current Input");
	}
}
