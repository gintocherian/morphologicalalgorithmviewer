/*
 * Copyright (c) 2004-2014 Ginto Cherian
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Interface for wrapping a matrix into image and vice versa.
 * Communicates between interface(Android/AWT) and algorithm.
 * For Android A = android.graphics.Bitmap
 * For AWT A = java.awt.image.BufferedImage
 */

package org.bitbucket.gintocherian.morphological;


public interface MatrixWrapper<A> {

	public int[][] getInput();

	public A getOutput();
	
	//Called from interface side
	public void changeInput(A input);

	//Called from algorithm side
	public void changeInput(AlgorithmOutput input);

}
