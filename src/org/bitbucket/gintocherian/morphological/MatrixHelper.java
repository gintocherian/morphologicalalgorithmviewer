/*
 * Copyright (c) 2014 Ginto Cherian
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/*
 * These are mostly generic matrix functions implemented for binary square matrices
 * Also note the image is represented by 0 and 1 is the free space
 *
 * This code has not been tested for arbitarily sized inputs and kernels
 * though the intention has been there. It has been tested for cases 
 * where,
 * 	Input = 50 X 50 matrix
 *  Display Output = 400 X 400 matrix or pixels
 *  Kernel = 3 X 3 matrix
 *
 */
package org.bitbucket.gintocherian.morphological;

public class MatrixHelper {

	//Print helper variable
	public static String newline = System.getProperty("line.separator");
	
	/*
	 * Checks if there is at least one non-zero element in the matrices
	 * after applying a padding of 1
	 */
	public static boolean checkNonZeroPresent(int[][] input, int size) {
		boolean present = false;
		for (int i = 1; i < size - 1; i++)
			for (int j = 1; j < size - 1; j++) {
				if (input[i][j] == 0) {
					present = true;
					break;
				}
			}
		return present;
	}

	/*
	 *  Combines the value in two matrices by using and operator
	 *  Note: the values are 0 which needs to be kept
	 */
	public static int[][] union(int[][] input1, int[][] input2, int size) {
		int[][] output = new int[size][size];
		for (int i = 0; i < size; i++)
			for (int j = 0; j < size; j++)
				output[i][j] = input1[i][j] & input2[i][j];
		return output;
	}

	/*
	 *  Computes the intersection between two matrices
	 */
	public static int[][] intersection(int[][] input1, int[][] input2, int size) {
		int[][] output = new int[size][size];
		for (int i = 0; i < size; i++)
			for (int j = 0; j < size; j++) {
				if (input1[i][j] == input2[i][j])
					output[i][j] = input1[i][j];
				else
					output[i][j] = 1;
			}
		return output;
	}

	/*
	 * Computes the difference between two matrices
	 */
	public static int[][] difference(int[][] mat1, int[][] mat2, int size) {
		int[][] output = new int[size][size];

		for (int i = 0; i < size; i++)
			for (int j = 0; j < size; j++) {
				output[i][j] = 1;
				if (mat1[i][j] != mat2[i][j])
					output[i][j] = 0;
			}
		return output;
	}

	/*
	 * Negative of given binary matrix
	 */
	public static int[][] complement(int[][] input, int size) {
		int[][] output = new int[size][size];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				output[i][j] = 1 - input[i][j];
			}
		}
		return output;
	}

	/*
	 * Removes border(1 unit wide) pixels for some operations
	 */
	public static int[][] clearBorder(int[][] input, int size) {
		
		for (int i = 0; i < size; i++)
			for (int j = 0; j < size; j++)
				if (i == 0 || j == 0 || i == (size-1) || j == (size-1))
					input[i][j] = 1;
		return input;
	}
	
	/*
	 * Gets the positive part of the matrix for the Hit_Miss operation
	 */
	public static int[][] extractPositiveKernel(int[][] input, int size) {
		int[][] output = new int[size][size];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (input[i][j] == 1)
					output[i][j] = 1;
				else
					output[i][j] = 0;
			}
		}
		return output;
	}

	/*
	 * Gets the negative part of the matrix for the Hit_Miss operation
	 */
	public static int[][] extractNegativeKernel(int[][] input, int size) {
		int[][] output = new int[size][size];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (input[i][j] == -1)
					output[i][j] = 1;
				else
					output[i][j] = 0;
			}
		}
		return output;
	}

	/*
	 * Used to convert the 50X50 array into pixellated looking 400X400 image
	 * input is a 50X50 matrix
	 * resizedMatrix is a 400X400 matrix
	 */
	public static int[][] enlargeMatrixEightTimes(int[][] input, int oldSize) {
		
		int newSize = oldSize*8;
		int[][] resizedMatrix = new int[newSize][newSize];
		int id = 0, i, j;
		for (i = 0; i < newSize; i++)
			for (j = 0; j < newSize; j++)
				resizedMatrix[i][j] = input[i / 8][j / 8];

		for (i = 0; i < newSize; i++) {
			id = 0;
			if ((i > 4)
					&& ((i % 8 == 0) || ((i + 1) % 8 == 0)
							|| ((i + 2) % 8 == 0) || ((i + 3) % 8 == 0))) {
				id = 1;
			}
			for (j = 0; j < newSize; j++) {
				if (id == 1) {
					resizedMatrix[i][j] = 1;
					resizedMatrix[j][i] = 1;
				}
			}
		}
		return resizedMatrix;
	}

	/*
	 * Returns false when matrices are different
	 */
	public static boolean compareMatrices(int[][] input1, int[][] input2, int size) {
		boolean output = true;
		for (int i = 0; i < size; i++)
			for (int j = 0; j < size; j++) {
				if (input1[i][j] != input2[i][j]) {
					output = false;
					break;
				}
			}
		return output;
	}
	
	
	/*
	 * A function for printing the any matrix - debugging
	 */
	public static String printMatrix(int[][] input, int size, String matrixName) {

		String matrix = "";
		matrix+="----<Starting printMatrix:" + matrixName + ">----" + newline;
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				matrix+= input[i][j] + " ";
			}
			matrix+= newline;
		}
		matrix+= "----<Ending printMatrix:" + matrixName + ">----" + newline;
		return matrix;
	}	
	
	/*
	 * A function for rotating a matrix clockwise n times at 90 degree intervals
	 * needs more testing, currently only tested for 2X2 & 3X3 matrix
	 */
	public static int[][]  rotateMatrix(int[][] input, int size, int n) {

		int ball;
		int elementOffset;
		int temp, last;
		for(int k = 0; k< n; k++) {
			
			elementOffset = 0;
			for(int i = size/2; i > 0; i--)	{
				
				ball = size - elementOffset*2 - 1;
				last = size - 1 - elementOffset;
				for(int j = 0; j < ball; j++)
				{
	
					temp = input[last - j][elementOffset];
					input[last - j][elementOffset] = input[last][last - j];
					input[last][last - j] = input[elementOffset + j][last];
					input[elementOffset + j][last] = input[elementOffset][elementOffset + j];
					input[elementOffset][elementOffset + j] = temp;
	
				}
				elementOffset += 1;
			}
		}
		return input;
	}	
}
