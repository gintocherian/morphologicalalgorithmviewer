Morphological binary image operations are a collection of operations designed to enhance the structures within the binary image based on some structuring elements. Good descriptions of these operations can be seen in, http://en.wikipedia.org/wiki/Mathematical_morphology. This app has been developed to demonstrate the working of the following morphological operations: - Erode, Dilate, Opening, Closing, HitMiss, Prune, Thin, Thick, Skeleton,       Boundary and ConvexHull.

The operations presented in this app have been developed based on descriptions provided in "Digital Image Processing (2nd Edition)" by "Rafael C. Gonzalez & Richard E. Woods". The structuring elements are 3X3 matrices.

The app uses 50X50 pixel sample images to run the operations on. The images can be selected by clicking the image icons. The images are then expanded so that their pixels can be clearly seen. 

Using the spinner, an operation can be selected to run. The operations though instantaneous, have been deliberately slowed to show intermediate results for the user to appreciate and learn these operations. While an operation is running the processing icon is displayed. Users can run operations any number of times as desired. To change the image or to reload, simply click the image icon.

The android app is present at, https://play.google.com/store/apps/details?id=org.bitbucket.gintocherian.android.morphological&hl=en